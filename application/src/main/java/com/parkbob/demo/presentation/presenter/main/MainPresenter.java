package com.parkbob.demo.presentation.presenter.main;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.parkbob.demo.domain.annotation.ActivityScope;
import com.parkbob.demo.domain.interactor.GetRulesContextUseCase;
import com.parkbob.demo.domain.rx.DefaultObserver;
import com.parkbob.demo.domain.rx.Params;
import com.parkbob.demo.presentation.presenter.base.BasePresenter;
import com.parkbob.demo.presentation.view.main.contract.ParkbobMvpContract;
import com.parkbob.models.RulesContext;

import javax.inject.Inject;

/**
 *  Main class moves away a logic from the view
 *  Created by mladen
 */
@ActivityScope
public class MainPresenter extends
        BasePresenter<ParkbobMvpContract.View>
        implements ParkbobMvpContract.Presenter {

    private static final String TAG = "MainPresenter";

    private GetRulesContextUseCase getRulesContextUseCase;

    @Inject
    public MainPresenter(@NonNull GetRulesContextUseCase getRulesContextUseCase) {
        this.getRulesContextUseCase = getRulesContextUseCase;
    }

    @Override
    public void detachView() {
        getRulesContextUseCase.dispose();
        super.detachView();
    }

    @Override
    public void refresh() {

    }

    /**
     * Initiates Async request for {@link RulesContext}
     * @param latLng
     */
    @Override
    public void getRulesContext(LatLng latLng) {
        Params params = Params.create();
        params.putLatLng(GetRulesContextUseCase.LAT_LNG, latLng);
        getRulesContextUseCase.execute(new RulesContextObserver(), params);
    }

    private final class RulesContextObserver extends DefaultObserver<RulesContext> {

        @Override
        public void onComplete() {
            getMvpView().hideProgress();
        }

        @Override
        public void onError(Throwable e) {
            getMvpView().hideProgress();
            getMvpView().showEmpty();
        }

        @Override
        public void onNext(RulesContext rulesContext) {
            getMvpView().onReceiveRuleContext(rulesContext);
        }
    }


}
