package com.parkbob.demo;

import com.google.android.gms.maps.model.LatLng;
import com.parkbob.demo.domain.interactor.GetRulesContextUseCase;
import com.parkbob.demo.domain.repository.ParkbobRepository;
import com.parkbob.demo.presentation.presenter.main.MainPresenter;
import com.parkbob.demo.presentation.view.main.contract.ParkbobMvpContract;
import com.parkbob.models.RulesContext;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Observable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by mladen
 */
@RunWith(MockitoJUnitRunner.class)
public class MainActivityPresenterTest {

    @Mock
    ParkbobMvpContract.View view;

    @Mock
    MainPresenter presenter;

    @Mock
    ParkbobRepository repository;

    @Mock private GetRulesContextUseCase getRulesContextUseCase;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp(){
        presenter = new MainPresenter(getRulesContextUseCase);
        presenter.attachView(view);
    }

    @Test
    public void testShouldPassRulesContextToView() {
        //when
        when(repository.getRulesContext(new LatLng(48.0, 29.0))).thenReturn(Observable.just(new RulesContext()));
        //given
        presenter.getRulesContext(new LatLng(48.0, 29.0));
        //then
        verify(view).showProgress();
        verify(view).onReceiveRuleContext(new RulesContext());
        verify(view).hideProgress();
    }

//    @Test
    public void testShouldFailWhenNoOrEmptyParameters() {
        expectedException.expect(NullPointerException.class);
    }

}
