package com.parkbob.demo.di.component;

import com.parkbob.demo.domain.annotation.ServiceScope;

import dagger.Component;

/**
 * Created by mladen
 */

@ServiceScope
@Component(dependencies = {ApplicationComponent.class})
public interface ServiceComponent {

}