package com.parkbob.demo.util;

/**
 * Created by mladen on 10/2/17.
 */

public class ObjectUtil {

    public static boolean isNull(Object obj) {
        return obj == null;
    }

    public static boolean isNotNull(Object obj) {
        return obj != null;
    }
}
