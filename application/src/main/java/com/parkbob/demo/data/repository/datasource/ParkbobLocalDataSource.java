package com.parkbob.demo.data.repository.datasource;

import com.google.android.gms.maps.model.LatLng;
import com.parkbob.demo.domain.annotation.ApplicationScope;
import com.parkbob.demo.domain.data.ParkbobDataSource;
import com.parkbob.models.RulesContext;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * A class for caching data
 * Created by mladen
 */
@ApplicationScope
public class ParkbobLocalDataSource implements ParkbobDataSource {

    private static final String TAG = "ParkbobLocalDataSource";

    @Inject
    public ParkbobLocalDataSource() {}

    @Override
    public Observable<RulesContext> getRulesContext(final LatLng latLng) {
        return Observable.empty();
    }


}