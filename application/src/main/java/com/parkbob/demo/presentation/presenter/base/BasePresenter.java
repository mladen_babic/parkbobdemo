package com.parkbob.demo.presentation.presenter.base;


import com.parkbob.demo.presentation.view.base.BaseView;

public class BasePresenter<T extends BaseView> implements IBasePresenter<T> {

    protected T view;

    @Override
    public void attachView(T view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void checkViewAttached() throws ViewNotAttachedException {
        if (!isViewAttached()) throw new ViewNotAttachedException();
    }

    private static class ViewNotAttachedException extends RuntimeException {
        ViewNotAttachedException() {
            super("Call Presenter.attachView(BaseView) before asking for data");
        }
    }

    public T getMvpView() {
        return view;
    }


}