package com.parkbob.demo.presentation.view.main.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.parkbob.demo.R;
import com.parkbob.demo.presentation.view.main.fragment.ParkRuleFragment;
import com.parkbob.models.GeoSpace;

/**
 * FragmentPagerAdapter responsible for showing tabs in the tab layout depending on TrafficRule.
 * If {@link GeoSpace} has both {@link com.parkbob.models.TrafficRule} ("now" and "later") then it shows two tabs
 * otherwise shows only one tab for current TrafficRule
 * Created by mladen
 */

public class ParkRuleFragmentPagerAdapter extends FragmentPagerAdapter {

    private final Context context;
    private final GeoSpace geoSpace;

    public ParkRuleFragmentPagerAdapter(Context context, GeoSpace geoSpace, FragmentManager fm) {
        super(fm);
        this.context = context;
        this.geoSpace = geoSpace;
    }

    /**
     * Position 0 indicates  ParkRuleFragment for current {@link com.parkbob.models.TrafficRule}
     * Position 2 indicates  ParkRuleFragment for next {@link com.parkbob.models.TrafficRule}
     * @param position indicates tap position
     * @return ParkRuleFragment
     */
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ParkRuleFragment.newInstance(geoSpace.getCurrentTrafficRule());
            case 1:
                return ParkRuleFragment.newInstance(geoSpace.getFutureTrafficeRule());
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return geoSpace.getFutureTrafficeRule() == null ?
                        context.getString(R.string.title_tab_always)
                        : context.getString(R.string.title_tab_now);
            case 1:
                return context.getString(R.string.title_tab_later);
            default:
                return context.getString(R.string.title_tab_now);
        }
    }

    @Override
    public int getCount() {
        return geoSpace.getFutureTrafficeRule() != null ? 2 : 1;
    }
}
