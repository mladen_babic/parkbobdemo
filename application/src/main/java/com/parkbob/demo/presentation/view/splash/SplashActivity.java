package com.parkbob.demo.presentation.view.splash;

import android.content.Intent;
import android.os.Bundle;

import com.parkbob.demo.presentation.view.base.BaseActivity;
import com.parkbob.demo.presentation.view.main.MainActivity;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
                Thread.sleep(2000);
                startMainActivity();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}
