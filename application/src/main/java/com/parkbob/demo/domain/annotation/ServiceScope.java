package com.parkbob.demo.domain.annotation;

import javax.inject.Scope;

/**
 * Indicates a Service scope.
 * All properties injected in the Service lives along with its Service
 * Created by mladen
 */
@Scope
public @interface ServiceScope {
}
