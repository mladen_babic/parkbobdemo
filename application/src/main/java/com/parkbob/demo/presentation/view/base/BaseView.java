package com.parkbob.demo.presentation.view.base;

public interface BaseView extends MvpView {

    void showError();
    void showEmpty();
    void showNoNetwork();

}