package com.parkbob.demo.presentation.view.base;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.Toast;

public abstract class BaseFragment
        extends Fragment
        implements BaseView {

    public Fragment getFragment() {
        return this;
    }
    public Context getContext() {
        return getActivity();
    }

    protected void showToastMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError() {

    }

    @Override
    public void showEmpty() {

    }

    @Override
    public void showNoNetwork() {

    }


}