# Parkbob Demo project

This project is just for reviewing Android skills and it is written in Clean Architecture approach.

### Installing

To get started and run the Android project, just clone the project from the BitBucket, compile and run it from Android Studio.

```
https://bitbucket.org/mladen_babic/parkbobdemo/
```

### How to use

On the first start, the app requires location permission to get your location.
First location is Vienna city and coordinates are hardcoded.


### Known problems

Due to bugs in the SDK, an app can not show routes for GeoSpace and points with icons on swipe

## Built With

* [Dagger2](https://google.github.io/dagger/) - The DI framework used
* [ButterKnife](https://github.com/JakeWharton/butterknife/) - Framework for injecting view 
* [RxJava](https://github.com/ReactiveX/RxJava) - Reactive Extensions for the JVM
* [OkHttp](http://square.github.io/okhttp/) - An HTTP & HTTP/2 client for Android and Java applications
* [Gson](https://github.com/google/gson) - A Java serialization/deserialization library to convert Java Objects into JSON and back
* [ViewPagerIndicator](https://github.com/vivchar/ViewPagerIndicator) - A Simple View Pager Indicator with animations
* Parkbob SDK

## Authors
* **Mladen Babic**