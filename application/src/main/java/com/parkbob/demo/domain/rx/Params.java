package com.parkbob.demo.domain.rx;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.Map;

/**
 */
public class Params {
    public static final Params EMPTY = Params.create();

    private final Map<String, Object> parameters = new HashMap<>();

    private Params() {
    }

    public static Params create() {
        return new Params();
    }

    public void putInt(String key,
                       int value) {
        parameters.put(key, value);
    }

    public int getInt(String key,
                      int defaultValue) {
        final Object object = parameters.get(key);
        if (object == null) {
            return defaultValue;
        }
        try {
            return (int) object;
        } catch (ClassCastException e) {
            return defaultValue;
        }
    }

    public void putString(String key,
                          String value) {
        parameters.put(key, value);
    }

    public String getString(String key,
                            String defaultValue) {
        final Object object = parameters.get(key);
        if (object == null) {
            return defaultValue;
        }
        try {
            return (String) object;
        } catch (ClassCastException e) {
            return defaultValue;
        }
    }

    public void putLong(String key,
                        long value) {
        parameters.put(key, value);
    }

    public long getLong(String key,
                        long defaultValue) {
        final Object object = parameters.get(key);
        if (object == null) {
            return defaultValue;
        }
        try {
            return (long) object;
        } catch (ClassCastException e) {
            return defaultValue;
        }
    }

    public void putLatLng(String key,
                          LatLng value) {
        parameters.put(key, value);
    }

    public LatLng getLatLng(String key,
                            LatLng defaultValue) {
        final Object object = parameters.get(key);
        if (object == null) {
            return defaultValue;
        }
        try {
            return (LatLng) object;
        } catch (ClassCastException e) {
            return defaultValue;
        }
    }
}