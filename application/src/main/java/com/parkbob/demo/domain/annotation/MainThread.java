package com.parkbob.demo.domain.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Indicates a main thread
 * Created by mladen
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface MainThread {
}
