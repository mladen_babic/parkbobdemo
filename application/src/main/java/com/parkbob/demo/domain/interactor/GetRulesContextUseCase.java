package com.parkbob.demo.domain.interactor;

import com.google.android.gms.maps.model.LatLng;
import com.parkbob.demo.domain.annotation.FragmentScope;
import com.parkbob.demo.domain.executor.PostExecutionThread;
import com.parkbob.demo.domain.executor.ThreadExecutor;
import com.parkbob.demo.domain.repository.ParkbobRepository;
import com.parkbob.demo.domain.rx.Params;
import com.parkbob.models.RulesContext;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * The class represents interactor  (use case) and holds a business logic.
 * It does not have any dependencies to Android platform
 * Created by mladen
 */
public class GetRulesContextUseCase extends UseCase<RulesContext, Params> {

    public static final String LAT_LNG =  "lat_lng";

    private final ParkbobRepository newsRepository;

    @Inject
    public GetRulesContextUseCase(ParkbobRepository newsRepository,
                                  ThreadExecutor threadExecutor,
                                  PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.newsRepository = newsRepository;
    }

    /**
     * Retrives Observable used for getting RulesContext
     * @param params
     * @return  Observable<RulesContext>
     */
    @Override
    Observable<RulesContext> buildUseCaseObservable(Params params) {
        LatLng latLng = params.getLatLng(LAT_LNG, null);
        return newsRepository.getRulesContext(latLng);
    }

}
