package com.parkbob.demo.domain.annotation;

import javax.inject.Scope;

/**
 * Indicates a Activity scope.
 * All properties injected in the Activity lives along with its Activity
 * Created by mladen
 */
@Scope
public @interface ActivityScope {
}
