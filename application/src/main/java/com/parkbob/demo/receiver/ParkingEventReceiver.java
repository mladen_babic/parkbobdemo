package com.parkbob.demo.receiver;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.parkbob.ParkbobConfiguration;
import com.parkbob.backend.entity.ParkingSpot;
import com.parkbob.models.RulesContext;

/**
 * This Receiver is responsible for handling event from Parkbob "engine"
 * Created by mladen
 */
public class ParkingEventReceiver extends WakefulBroadcastReceiver {

    private static final String TAG = "ParkingEventReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        //Information about nearby parking regulations.
        if (intent.hasExtra(ParkbobConfiguration.RECEIVER_PARKING_EVENT_OCCURRED_EXTRA_RULES_CONTEXT)) {
            RulesContext rulesContext = (RulesContext) intent.getSerializableExtra(ParkbobConfiguration.RECEIVER_PARKING_EVENT_OCCURRED_EXTRA_RULES_CONTEXT);
//            showNotification(rulesContext);/
            Log.d(TAG, "show notification");
        }

        //Information about the parked car.
        if (intent.hasExtra(ParkbobConfiguration.RECEIVER_PARKING_EVENT_OCCURRED_EXTRA_PARKING_SPOT)) {
            ParkingSpot parkingSpot = (ParkingSpot) intent.getParcelableExtra(ParkbobConfiguration.RECEIVER_PARKING_EVENT_OCCURRED_EXTRA_PARKING_SPOT);
//            placeMarkerOnMap(parkingSpot);
            Log.d(TAG, "show parking info");
        }
    }
}
