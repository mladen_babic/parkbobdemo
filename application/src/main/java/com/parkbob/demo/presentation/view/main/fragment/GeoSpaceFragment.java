package com.parkbob.demo.presentation.view.main.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parkbob.demo.R;
import com.parkbob.demo.presentation.view.main.adapter.ParkRuleFragmentPagerAdapter;
import com.parkbob.models.GeoSpace;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * The fragment creates pages for geospaces.
 */
public class GeoSpaceFragment extends Fragment {

    private final static String GEO_SPACE = "GEO_SPACE";

    private GeoSpace geoSpace;
    private Unbinder unbinder;

    @BindView(R.id.tl_park_rules)
    TabLayout tabLayoutParkRule;

    @BindView(R.id.vp_park_rule)
    com.parkbob.demo.presentation.view.main.adapter.CustomViewPager viewPager;

    public GeoSpaceFragment() {
        // Required empty public constructor
    }

    public static GeoSpaceFragment newInstance(GeoSpace geoSpace) {
        GeoSpaceFragment fragment = new GeoSpaceFragment();
        Bundle args = new Bundle();
        args.putSerializable(GEO_SPACE, geoSpace);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            geoSpace = (GeoSpace) getArguments().getSerializable(GEO_SPACE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_geo_spaces, container, false);
        unbinder = ButterKnife.bind(this, view);
        viewPager.setPagingEnabled(false);

        ParkRuleFragmentPagerAdapter adapter = new ParkRuleFragmentPagerAdapter(getActivity(),
                geoSpace, getChildFragmentManager());
        viewPager.setAdapter(adapter);

        tabLayoutParkRule.post(new Runnable() {
            @Override
            public void run() {
                tabLayoutParkRule.setupWithViewPager(viewPager);
            }
        });

        return view;
    }

    /**
     * Getter for retrieving assigned {@link GeoSpace} for this {@link GeoSpaceFragment}
     * @return {@link GeoSpace}
     */
    public GeoSpace getGeoSpace() {
        return geoSpace;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


}
