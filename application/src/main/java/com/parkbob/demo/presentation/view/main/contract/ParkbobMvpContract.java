package com.parkbob.demo.presentation.view.main.contract;

import com.google.android.gms.maps.model.LatLng;
import com.parkbob.demo.presentation.presenter.base.IBasePresenter;
import com.parkbob.models.RulesContext;

/**
 * A class uses as contract between View and Presenter in MVP architecture
 */
public interface ParkbobMvpContract {

    interface View extends com.parkbob.demo.presentation.view.base.BaseView {
        void showProgress();
        void hideProgress();
        void onReceiveRuleContext(RulesContext rulesContext);
    }

    interface Presenter extends IBasePresenter<View> {
        void refresh();
        void getRulesContext(LatLng latLng);
    }


}
