package com.parkbob.demo.data.repository.datasource;

import com.google.android.gms.maps.model.LatLng;
import com.parkbob.ParkbobManager;
import com.parkbob.demo.domain.annotation.ApplicationScope;
import com.parkbob.demo.domain.data.ParkbobDataSource;
import com.parkbob.models.RulesContext;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * This class is responsible for retrieving {@link RulesContext} data from web network
 * Created by mladen
 */
@ApplicationScope
public class ParkbobRemoteDataSource implements ParkbobDataSource {

    private static final String TAG = "ParkbobRemoteDataSource";
    private ParkbobManager parkbobManager;

    @Inject
    public ParkbobRemoteDataSource(ParkbobManager parkbobManager) {
        this.parkbobManager = parkbobManager;
    }

    @Override
    public Observable<RulesContext> getRulesContext(final LatLng latLng) {
        return Observable.create(new ObservableOnSubscribe<RulesContext>() {
            @Override
            public void subscribe(ObservableEmitter<RulesContext> emitter) throws Exception {
                try {
                    RulesContext rulesContext = parkbobManager.getRulesContext(latLng);
                    emitter.onNext(rulesContext);
                    emitter.onComplete();
                } catch (Exception e) {
                    emitter.onError(e);
                }
            }
        });
    }
}
