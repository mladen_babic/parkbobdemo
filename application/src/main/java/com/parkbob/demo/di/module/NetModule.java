package com.parkbob.demo.di.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.parkbob.demo.domain.annotation.ApplicationScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mladen
 */
@Module
public class NetModule {

    @Provides
    @ApplicationScope
    public Gson provideGson() {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .setLenient()
                .create();
    }
}
