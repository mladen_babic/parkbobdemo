package com.parkbob.demo.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.parkbob.demo.domain.annotation.ApplicationContext;
import com.parkbob.demo.domain.annotation.ApplicationScope;

import javax.inject.Inject;

/**
 * This class is used for saving data in the {@link SharedPreferences}
 * It lives in the {@link ApplicationScope}
 */
@ApplicationScope
public class PreferencesUtil {

    private static final String TAG = "PreferencesUtil";
    final SharedPreferences sp;

    @Inject
    public PreferencesUtil(@ApplicationContext Context context) {
        sp = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void savePrefs(String key, boolean value) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean(key, value);
        edit.apply();
    }

    public void savePrefs(String key, int value) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt(key, value);
        edit.apply();
    }

    public void savePrefs(String key, long value) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putLong(key, value);
        edit.apply();
    }

    public void savePrefs(String key, float value) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putFloat(key, value);
        edit.apply();
    }

    public void savePrefs(String key, String value) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(key, value);
        edit.apply();
    }

    public String getPrefs(String key, String defaultValue) {
        return sp.getString(key, defaultValue);
    }

    public int getPrefs(String key, int defaultValue) {
        return sp.getInt(key, defaultValue);
    }

    public float getPrefs(String key, float defaultValue) {
        return sp.getFloat(key, defaultValue);
    }

    public long getPrefs(String key, long defaultValue) {
        return sp.getLong(key, defaultValue);
    }

    public boolean getPrefs(String key, boolean defaultValue) {
        return sp.getBoolean(key, defaultValue);
    }

}