package com.parkbob.demo.presentation.view.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.vivchar.viewpagerindicator.ViewPagerIndicator;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parkbob.ParkbobManager;
import com.parkbob.backend.entity.ParkingEventType;
import com.parkbob.demo.R;
import com.parkbob.demo.application.ParkbobApp;
import com.parkbob.demo.domain.annotation.ApplicationContext;
import com.parkbob.demo.presentation.presenter.main.MainPresenter;
import com.parkbob.demo.presentation.view.base.BaseActivity;
import com.parkbob.demo.presentation.view.main.adapter.GeoSpaceFragmentPagerAdapter;
import com.parkbob.demo.presentation.view.main.contract.ParkbobMvpContract;
import com.parkbob.demo.presentation.view.main.fragment.GeoSpaceFragment;
import com.parkbob.demo.util.PreferencesUtil;
import com.parkbob.models.GeoSpace;
import com.parkbob.models.Geometry;
import com.parkbob.models.RulesContext;
import com.parkbob.service.util.DeviceSupported;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends BaseActivity
        implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener,
        ParkbobMvpContract.View {

    private static final String TAG = "MainActivity";
    private static final int RC_REQUEST = 10000;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Inject
    ParkbobApp application;

    @Inject
    ParkbobManager parkbobManager;

    @Inject
    @ApplicationContext
    Context context;

    @Inject
    PreferencesUtil preferencesUtil;

    @Inject
    MainPresenter presenter;

    @BindView(R.id.park_rule_container)
    RelativeLayout relativeLayoutParkRuleContainer;

    @BindView(R.id.tv_park_rule_address)
    TextView textViewParkRuleAddress;

    @BindView(R.id.tv_park_rule_distance)
    TextView textViewParkRuleDistance;

    @BindView(R.id.iv_navigation)
    ImageView imageViewNavigation;

    @BindView(R.id.vp_geo_spaces)
    ViewPager viewPagerGeoSpace;

    @BindView(R.id.pb_park_rule_data)
    ProgressBar progressBarParkRule;

    private GoogleMap mMap;
    private Unbinder unbinder;
    private BottomSheetBehavior mBottomSheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getComponent().inject(this);
        unbinder = ButterKnife.bind(this);
        setToolbar();

        presenter.attachView(this);

        parkbobManager.bindService(this);

        requestLocationPermission();
        checkDeviceSupport();

        parkbobManager.activateRecognitionEngine();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        relativeLayoutParkRuleContainer.setVisibility(View.INVISIBLE);

        mBottomSheetBehavior = BottomSheetBehavior.from(relativeLayoutParkRuleContainer);
        mBottomSheetBehavior.setPeekHeight(180);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady() called with: googleMap = [" + googleMap + "]");
        mMap = googleMap;
        LatLng vienna = new LatLng(48.2082D, 16.3738D);
        mMap.addMarker(new MarkerOptions().position(vienna).title("Marker in Vienna"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(vienna));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(vienna, 15));
        mMap.setOnMapClickListener(this);
        presenter.getRulesContext(vienna);
    }


    /**
     * Method action to test simulation parking event
     */
    @OnClick(R.id.test_btn)
    public void test() {
        Location viennaLocation = new Location("demo");
        viennaLocation.setLatitude(48.2082D);
        viennaLocation.setLongitude(16.3738D);
        parkbobManager.simulateParkingEvent(viennaLocation, ParkingEventType.IN);

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * The method requests for location permission
     */
    private void requestLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MainActivity.RC_REQUEST);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d("PermAppIntroFragment", "onRequestPermissionsResult() called with: requestCode = [" + requestCode + "], permissions = [" + permissions + "], grantResults = [" + grantResults + "]");
        parkbobManager.activateRecognitionEngine();
    }

    /**
     * It checks whether a device has support for a RecognitionEngine
     * It is currently just for test purpose
     */
    public void checkDeviceSupport() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final DeviceSupported state = ParkbobManager.getInstance().getIsDeviceSupportedByRecognitionEngineBlocking(30000);
                Log.d(TAG, "MainActivity: " + state.name());
            }
        }).start();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    /**
     * It is a method which is invoked by tapping on the map.
     * it requests for par rules for particular coordinates
     * @param latLng LatLng
     */
    @Override
    public void onMapClick(LatLng latLng) {
        Log.d(TAG, "onMapClick() called with: latLng = [" + latLng + "]");
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(latLng));
        presenter.getRulesContext(latLng);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
    }

    @Override
    public void showProgress() {
        progressBarParkRule.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBarParkRule.setVisibility(View.GONE);
    }

    /**
     * This method has responsibility for retrieving and set data to fields in the bottom sheet (park rule views)
     * The method is called from MainPresenter which is also has role for retrieving data.
     * It creates view pagers for geospaces and traffic rules tabs and shows those data on the google map by swiping geospaces
     * @param rulesContext RulesContext
     */
    @Override
    public void onReceiveRuleContext(final RulesContext rulesContext) {

        if (rulesContext != null) {

            setCoordinatesForNavigationAction(rulesContext);

            if (rulesContext.getAddress() != null) {
                textViewParkRuleAddress.setText(rulesContext.getAddress().getDisplayAddress());
            }

            final List<GeoSpace> geoSpaceList = rulesContext.getGeoSpaceList();

            if (!geoSpaceList.isEmpty()) {

                final GeoSpaceFragmentPagerAdapter adapter = new GeoSpaceFragmentPagerAdapter(geoSpaceList, getSupportFragmentManager());
                viewPagerGeoSpace.setVisibility(View.VISIBLE);
                viewPagerGeoSpace.setAdapter(adapter);

                ViewPagerIndicator indicator = (ViewPagerIndicator) findViewById(R.id.indicator);
                indicator.setupWithViewPager(viewPagerGeoSpace);

                viewPagerGeoSpace.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    public void onPageScrollStateChanged(int state) {
                    }

                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    public void onPageSelected(int position) {

                        clearAllLinesAndPoints(geoSpaceList);

                        Log.d(TAG, "onPageSelected: " + position);
                        GeoSpaceFragment geoSpaceFragment = adapter.getRegisteredFragment(position);
                        if (geoSpaceFragment != null) {
                            setGeoSpaceData(geoSpaceFragment.getGeoSpace());
                        }
                    }
                });
            } else {
                viewPagerGeoSpace.setVisibility(View.GONE);
            }

            relativeLayoutParkRuleContainer.setVisibility(View.VISIBLE);
            mBottomSheetBehavior.setState(geoSpaceList.isEmpty() ? BottomSheetBehavior.STATE_COLLAPSED : BottomSheetBehavior.STATE_EXPANDED);

//            Log.d(TAG, "onReceiveRuleContext() called with: rulesContext = [" + rulesContext + "]");
//            Log.w(TAG, "onMapClick: Address : " + rulesContext.getAddress().getDisplayAddress());
//            Log.w(TAG, "onMapClick: Radius : " + rulesContext.getRadius());
//            Log.w(TAG, "onMapClick: Date : " + rulesContext.getTimeStamp());

            if (!geoSpaceList.isEmpty()) {
                GeoSpace geoSpace = geoSpaceList.get(0);
                setGeoSpaceData(geoSpace);
            }
        }
    }

    /**
     * The method sets listener and action for navigation image located on the bottom sheet.
     * @param rulesContext RulesContext
     */
    private void setCoordinatesForNavigationAction(final RulesContext rulesContext) {
        imageViewNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "google.navigation:q={lat}, {lng}".replace("{lat}",
                        String.valueOf(rulesContext.getLocation().getLatLng().latitude))
                        .replace("{lng}",
                                String.valueOf(rulesContext.getLocation().getLatLng().longitude));

                Uri gmmIntentUri = Uri.parse(url);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
    }

    /**
     * It clears all previous lines and points on the map
     * @param geoSpaceList List<GeoSpace>
     */
    private void clearAllLinesAndPoints(List<GeoSpace> geoSpaceList) {
        for (GeoSpace geoSpace : geoSpaceList) {
            for (Geometry geometry : geoSpace.getGeometries()) {
                if (geometry != null) {
                    geometry.clearFromMap();
                }
            }
        }
    }

    /**
     * The method shows a distance and draws lines and points depends on Geometry type.
     * @param geoSpace {@link GeoSpace}
     */
    private void setGeoSpaceData(GeoSpace geoSpace) {

        textViewParkRuleDistance.setText(getString(R.string.distance_unit, geoSpace.getDistance()));

        if (mMap != null) {
            for (final Geometry geometry : geoSpace.getGeometries()) {
                Log.d(TAG, "onReceiveRuleContext: geometry: " + geometry);
                if (geometry != null) {
                    //TODO Mladen drawing doesn't work. It never draws lines. It draws points only partly (without icons)
                    geometry.drawOnMap(mMap, ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        presenter.detachView();
    }
}
