package com.parkbob.demo.domain.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 *  Indicates that property lives until lives {@link android.app.Application}
 * Created by mladen
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ApplicationScope {

}
