package com.parkbob.demo.domain.data;

import com.google.android.gms.maps.model.LatLng;
import com.parkbob.models.RulesContext;

import io.reactivex.Observable;

/**
 *
 * Interface with methods which are implemented in Repository and DataSource classes
 * Created by mladen
 */

public interface ParkbobDataSource {

    Observable<RulesContext> getRulesContext(LatLng latLng);

}
