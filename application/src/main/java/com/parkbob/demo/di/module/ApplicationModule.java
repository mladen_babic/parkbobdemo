package com.parkbob.demo.di.module;

import android.content.Context;

import com.parkbob.ParkbobManager;
import com.parkbob.demo.application.ParkbobApp;
import com.parkbob.demo.domain.annotation.ApplicationContext;
import com.parkbob.demo.domain.annotation.ApplicationScope;
import com.parkbob.demo.domain.annotation.BackgroundThread;
import com.parkbob.demo.domain.annotation.MainThread;
import com.parkbob.demo.domain.executor.JobExecutor;
import com.parkbob.demo.domain.executor.PostExecutionThread;
import com.parkbob.demo.domain.executor.ThreadExecutor;
import com.parkbob.demo.domain.executor.UIThread;

import java.util.Stack;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * A module to wrap the Application state and expose it to the graph.
 */
@Module
public class ApplicationModule {

    private ParkbobApp application;

    public ApplicationModule(ParkbobApp application) {
        this.application = application;
    }

    @Provides
    @ApplicationScope
    @ApplicationContext
    public Context provideApplicationContext() {
        return this.application;
    }

    @Provides
    @ApplicationScope
    public ParkbobApp provideApplication() {
        return application;
    }

    @Provides
    @MainThread
    public Scheduler provideMainThread() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @BackgroundThread
    public Scheduler provideBackgroundThread() {
        return Schedulers.io();
    }

    @Provides
    @ApplicationScope
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @ApplicationScope
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @ApplicationScope
    Stack<Long> provideSelectedEditions() {
        return new Stack<>();
    }

    @Provides
    @ApplicationScope
    ParkbobManager provideParbobManager(){
        return ParkbobManager.getInstance();
    }

}
