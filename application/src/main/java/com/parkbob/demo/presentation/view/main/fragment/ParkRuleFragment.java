package com.parkbob.demo.presentation.view.main.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parkbob.demo.R;
import com.parkbob.models.ParkingCost;
import com.parkbob.models.TrafficRule;

import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * This fragment is responsible for showing traffic rule. Traffic Rule can be a "now" or "later"
 */
public class ParkRuleFragment extends Fragment {

    private final static String TRAFFIC_RULE = "TRAFFIC_RULE";
    private static final String TAG = "ParkRuleFragment";

    @BindView(R.id.tv_item_rule_name)
    TextView textViewParkRuleName;

    @BindView(R.id.tv_item_type_cost)
    TextView textViewParkRuleCost;

    @BindView(R.id.ll_item_rule_info_container)
    LinearLayout linearLayoutRuleInfo;

    @BindView(R.id.iv_item_type_status)
    ImageView imageViewRuleStatus;

    @BindView(R.id.tv_item_type_parking_status)
    TextView textViewRuleStatus;

    @BindView(R.id.tv_item_type_start_time)
    TextView textViewRuleStartTime;

    @BindView(R.id.tv_item_type_end_time)
    TextView textViewRuleEndTime;

    @BindView(R.id.tv_item_type_max_stay_value)
    TextView textViewRuleMaxStay;

    @BindView(R.id.tv_item_type_max_stay_lbl)
    TextView textViewRuleMaxStayLbl;

    private TrafficRule trafficRule;
    private Unbinder unbinder;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd.MM.yy | HH:mm", Locale.US);

    public ParkRuleFragment() {
        // Required empty public constructor
    }

    /**
     * Static method for creating a instance of ParkRuleFragment
     * @param trafficRule  TrafficRule
     * @return ParkRuleFragment a new instance
     */
    public static ParkRuleFragment newInstance(TrafficRule trafficRule) {
        ParkRuleFragment fragment = new ParkRuleFragment();
        Bundle args = new Bundle();
        args.putSerializable(TRAFFIC_RULE, trafficRule);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            trafficRule = (TrafficRule) getArguments().getSerializable(TRAFFIC_RULE);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_park_rule, container, false);
        unbinder = ButterKnife.bind(this, view);
        setRulesData();
        return view;
    }

    /**
     *  The method is responsible for showing traffic rule data
     */
    private void setRulesData() {

        // TODO: Mladen IT DOESN'T SET A COLOR PROPERLY. POSSIBLE A SDK BUG
        linearLayoutRuleInfo.setBackgroundColor(trafficRule.getRuleColor(getActivity()));
        textViewParkRuleName.setText(trafficRule.getRuleName());

        if (trafficRule.getParkingCost() != null && !trafficRule.getParkingCost().isEmpty()) {
            ParkingCost parkingCost = trafficRule.getParkingCost().get(0);
            textViewParkRuleCost.setText(TextUtils.concat(parkingCost.getPrice() + " ",
                    parkingCost.getCurrencySymbol()));
            textViewRuleStatus.setText(R.string.title_park_rule_charges_apply);
        } else {
            textViewRuleStatus.setText(R.string.title_park_rule_free_of_charge);
        }

        if (trafficRule.getMaxStay() > 0) {
            textViewRuleMaxStay.setText(getString(R.string.title_max_stay_value, trafficRule.getMaxStay()));
            textViewRuleMaxStayLbl.setVisibility(View.VISIBLE);
        } else {
            textViewRuleMaxStayLbl.setVisibility(View.GONE);
        }

        if (trafficRule.getStartTime() != null && trafficRule.getEndTime() != null) {
            textViewRuleStartTime.setText(simpleDateFormat.format(trafficRule.getStartTime()));
            textViewRuleEndTime.setText(simpleDateFormat.format(trafficRule.getEndTime()));
        }

        setRuleSign();
    }

    /**
     * It sets image sign for traffic rule depending on traffic type
     */
    private void setRuleSign() {
        switch (trafficRule.getType()) {
            case PUBLIC:
            case PREFERRED:
                imageViewRuleStatus.setImageResource(R.drawable.ic_status_parking);
                break;
            case NO_PARKING:
                textViewRuleStatus.setText(R.string.title_park_rule_no_parking);
                imageViewRuleStatus.setImageResource(R.drawable.status_parking_prohibited);
                break;
            case UNREGULATED:
            case NO_STREET_NEAR:
                imageViewRuleStatus.setImageResource(R.drawable.status_parking_prohibited);
                textViewRuleStatus.setText(R.string.title_park_rule_restricted_zone);
                break;
            default:
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
