package com.parkbob.demo.di.component;

import com.parkbob.ParkbobManager;
import com.parkbob.demo.di.module.ActivityModule;
import com.parkbob.demo.domain.annotation.ActivityScope;
import com.parkbob.demo.domain.annotation.BackgroundThread;
import com.parkbob.demo.domain.annotation.MainThread;
import com.parkbob.demo.domain.executor.PostExecutionThread;
import com.parkbob.demo.domain.executor.ThreadExecutor;
import com.parkbob.demo.domain.repository.ParkbobRepository;
import com.parkbob.demo.presentation.view.main.MainActivity;
import com.parkbob.demo.presentation.view.splash.SplashActivity;
import com.parkbob.demo.util.PreferencesUtil;

import dagger.Component;
import io.reactivex.Scheduler;

/**
 *
 * This component class is used for injecting dependencies in Activity classes
 * It exposes methods from {@link ApplicationComponent}
 * Created by mladen
 */
@ActivityScope
@Component(modules = ActivityModule.class, dependencies = {ApplicationComponent.class})
public interface ActivityComponent {

    void inject(MainActivity mainActivity);
    void inject(SplashActivity splashActivity);

    ParkbobRepository newsRepository();
    PreferencesUtil preferencesUtil();

    @MainThread
    Scheduler mainThread();

    @BackgroundThread
    Scheduler backgroundThread();

    ThreadExecutor threadExecutor();
    PostExecutionThread postExecutionThread();
    ParkbobManager provideParbobManager();
}
