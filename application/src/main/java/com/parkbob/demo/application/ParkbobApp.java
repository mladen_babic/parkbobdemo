package com.parkbob.demo.application;

import android.app.Activity;
import android.app.IntentService;
import android.support.multidex.MultiDexApplication;

import com.parkbob.ParkbobManager;
import com.parkbob.demo.di.component.ApplicationComponent;
import com.parkbob.demo.di.component.DaggerApplicationComponent;
import com.parkbob.demo.di.module.ApplicationModule;


public class ParkbobApp extends MultiDexApplication {

    private ApplicationComponent applicationComponent;

    public static ParkbobApp get(Activity activity) {
        return (ParkbobApp) activity.getApplication();
    }

    public static ParkbobApp get(IntentService service) {
        return (ParkbobApp) service.getApplication();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        ParkbobManager.initInstance(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
