package com.parkbob.demo.data.repository;

import com.google.android.gms.maps.model.LatLng;
import com.parkbob.demo.data.repository.datasource.ParkbobLocalDataSource;
import com.parkbob.demo.data.repository.datasource.ParkbobRemoteDataSource;
import com.parkbob.demo.domain.annotation.ApplicationScope;
import com.parkbob.demo.domain.data.ParkbobDataSource;
import com.parkbob.demo.domain.repository.ParkbobRepository;
import com.parkbob.models.RulesContext;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * It implements methods sued for retrieving RulesContext
 * Created by mladen
 */
@ApplicationScope
public class ParkbobRepositoryImpl implements ParkbobRepository {

    private static final String TAG = "ParkbobRepositoryImpl";

    ParkbobDataSource newsRemoteDataSource;
    ParkbobLocalDataSource newsLocalDataSource;

    @Inject
    public ParkbobRepositoryImpl(ParkbobRemoteDataSource newsRemoteDataSource,
                                 ParkbobLocalDataSource newsLocalDataSource) {
        this.newsRemoteDataSource = newsRemoteDataSource;
        this.newsLocalDataSource = newsLocalDataSource;
    }

    @Override
    public Observable<RulesContext> getRulesContext(LatLng latLng) {
        return newsRemoteDataSource.getRulesContext(latLng);
    }

}