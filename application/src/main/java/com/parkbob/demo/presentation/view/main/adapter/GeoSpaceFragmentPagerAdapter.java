package com.parkbob.demo.presentation.view.main.adapter;

import android.support.v4.app.FragmentManager;

import com.parkbob.demo.presentation.view.main.fragment.GeoSpaceFragment;
import com.parkbob.models.GeoSpace;

import java.util.ArrayList;
import java.util.List;

/**
 * PagerAdapter responsible for showing {@link GeoSpace}
 */
public class GeoSpaceFragmentPagerAdapter extends SmartFragmentStatePagerAdapter<GeoSpaceFragment> {

    private List<GeoSpace> geoSpaceList = new ArrayList<>();

    public GeoSpaceFragmentPagerAdapter(List<GeoSpace> geoSpaceList, FragmentManager fm) {
        super(fm);
        this.geoSpaceList = geoSpaceList;
    }

    @Override
    public GeoSpaceFragment getItem(int position) {
        GeoSpace geoSpace = geoSpaceList.get(position);
        return GeoSpaceFragment.newInstance(geoSpace);
    }

    @Override
    public int getCount() {
        return geoSpaceList.size();
    }
}
