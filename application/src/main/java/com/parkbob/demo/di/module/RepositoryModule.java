package com.parkbob.demo.di.module;

import android.content.Context;
import android.util.Log;

import com.parkbob.demo.data.repository.ParkbobRepositoryImpl;
import com.parkbob.demo.domain.annotation.ApplicationContext;
import com.parkbob.demo.domain.annotation.ApplicationScope;
import com.parkbob.demo.domain.repository.ParkbobRepository;
import com.parkbob.demo.util.PreferencesUtil;

import dagger.Module;
import dagger.Provides;

/**
 *
 * It provides ParkbobRepositor and PreferencesUtil in the {@link ApplicationScope}
 * Created by mladen
 */
@Module
public class RepositoryModule {

    private static final String TAG = "RepositoryModule";

    @Provides
    @ApplicationScope
    public ParkbobRepository provideRepository(ParkbobRepositoryImpl newsRepository) {
        Log.d(TAG, "provideRepository: ");
        return newsRepository;
    }

    @Provides
    @ApplicationScope
    public PreferencesUtil providePref(@ApplicationContext Context context) {
        return new PreferencesUtil(context);
    }

}