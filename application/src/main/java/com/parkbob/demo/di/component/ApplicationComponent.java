package com.parkbob.demo.di.component;

import android.content.Context;

import com.parkbob.ParkbobManager;
import com.parkbob.demo.application.ParkbobApp;
import com.parkbob.demo.domain.annotation.ApplicationContext;
import com.parkbob.demo.domain.annotation.ApplicationScope;
import com.parkbob.demo.domain.annotation.BackgroundThread;
import com.parkbob.demo.domain.annotation.MainThread;
import com.parkbob.demo.di.module.ApplicationModule;
import com.parkbob.demo.di.module.NetModule;
import com.parkbob.demo.di.module.RepositoryModule;
import com.parkbob.demo.domain.executor.PostExecutionThread;
import com.parkbob.demo.domain.executor.ThreadExecutor;
import com.parkbob.demo.domain.repository.ParkbobRepository;
import com.parkbob.demo.util.PreferencesUtil;

import java.util.Stack;

import dagger.Component;
import io.reactivex.Scheduler;

/**
 *
 * This component class is used for injecting dependencies in the Application class
 * and injected data lives along with app class
 * Created by mladen
 */
@ApplicationScope
@Component(modules = {ApplicationModule.class, NetModule.class, RepositoryModule.class})
public interface ApplicationComponent {

    @ApplicationContext
    Context context();

    ParkbobApp myApp();

    ParkbobRepository newsRepository();

    PreferencesUtil preferencesUtil();

    @MainThread
    Scheduler mainThread();

    @BackgroundThread
    Scheduler backgroundThread();

    ThreadExecutor threadExecutor();
    PostExecutionThread postExecutionThread();
    ParkbobManager provideParkbobManager();


}
